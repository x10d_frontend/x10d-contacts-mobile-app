import 'react-native-gesture-handler'
// https://github.com/mobxjs/mobx-react-lite/blob/master/README.md#observer-batching
import 'mobx-react/batchingForReactNative'
import { AppRegistry } from 'react-native'
import { enableScreens } from 'react-native-screens'

import App from './src/App'
import { name as appName } from './app.json'


enableScreens()

AppRegistry.registerComponent(appName, () => App)
