module.exports = {
    presets: [
        'module:metro-react-native-babel-preset',
    ],
    plugins: [
        ['@babel/plugin-proposal-decorators', { legacy: true }],
        [
            'module-resolver',
            {
                root: ['./src'],
                extensions: [
                    '.ts',
                    '.tsx',
                    '.js',
                    '.jsx',
                    '.json',
                ],
                alias: {
                    'common-components': './src/common-components',
                    components: './src/components',
                    interfaces: './src/interfaces',
                    helpers: './src/util',
                    models: './src/models',
                    navigation: './src/navigation',
                    screens: './src/screens',
                    'screen-names': './src/screens/screen-names',
                    services: './src/services',
                    styles: './src/styles',
                },
            },
        ],
    ],
}
