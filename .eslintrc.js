module.exports = {
    root: true,

    env: {
        node: true,
    },

    extends: [
        '@x10d',
        '@x10d/eslint-config/typescript',
        '@x10d/eslint-config/react',
    ],

    settings: {
    // Объектная форма, чтобы значение мержилось с объектами
    // расширяемых конфигов и использовались все резолверы в конечной конфигурации
        'import/resolver': {
            // (eslint-import-resolver-react-native)
            'react-native': true,
        },
    },

    plugins: [
        'react-native',
    ],

    globals: {
        navigator: false,
    },

    rules: {
        camelcase: 'off',
        '@typescript-eslint/camelcase': 'off',

        'arrow-body-style': 'off',

        'no-underscore-dangle': 'off',

        'react/prop-types': ['error', {
            skipUndeclared: true,
        }],

        // Было бы полезно, но работает только со стилями в `StyleSheet.create`,
        // объявленными в том же файле, что и компонент
        'react-native/no-unused-styles': 'error',

        'react-native/no-color-literals': 'error',

        // Иногда бывает нужно
        'react-native/no-inline-styles': 'warn',

        // Отключаем, так как используем компонент обертку над RN Text
        'react-native/no-raw-text': 'off',

        '@typescript-eslint/explicit-function-return-type': 'warn',
    },

    overrides: [
    // Константа, предоставляемая сборщиком RN, для RN кода
        {
            files: [
                'index.js',
                'src/**',
            ],
            globals: {
                __DEV__: false,
            },
        },

        {
            files: [
                '__tests__/**',
            ],
            env: {
                jest: true,
            },
        },

        {
            files: [
                'src/models/**',
            ],
            rules: {
                // Особыенные для MST интерфейсы должны быть обязательно
                // интерфейсами, а не типами
                '@typescript-eslint/no-empty-interface': 'off',
            },
        },
    ],
}
