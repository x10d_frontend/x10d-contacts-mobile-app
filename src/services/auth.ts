import auth, { FirebaseAuthTypes } from '@react-native-firebase/auth'


export class FirebaseAuth {
    public currentUser : FirebaseAuthTypes.User | null = null

    public async setup() : Promise<void> {
        return new Promise((resolve) => {
            auth().onAuthStateChanged((user) => {
                this.currentUser = user

                resolve()
            })
        })
    }

    public async signInAnonymously() : Promise<void> {
        await auth().signInAnonymously()
    }
}
