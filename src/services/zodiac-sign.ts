import { ImageSourcePropType } from 'react-native'
import { addYears, setYear, startOfDay, endOfDay, isWithinInterval, Interval } from 'date-fns'
import { parseWithOptions } from 'date-fns/fp'
import { Maybe } from 'purify-ts'

import { parsedDatabaseDate } from 'helpers'


enum ZodiacSign {
    ARIES = 'ARIES',
    TAURUS = 'TAURUS',
    GEMINI = 'GEMINI',
    CANCER = 'CANCER',
    LEO = 'LEO',
    VIRGO = 'VIRGO',
    LIBRA = 'LIBRA',
    SCORPIO = 'SCORPIO',
    SAGITTARIUS = 'SAGITTARIUS',
    CAPRICORN = 'CAPRICORN',
    AQUARIUS = 'AQUARIUS',
    PISCES = 'PISCES',
}

interface Zodiac {
    id : ZodiacSign
    name : string
    image : ImageSourcePropType
    interval : Interval
}


const rangeDay = parseWithOptions({}, new Date(), 'dd-MM')

const zodiacMappings : Array<Zodiac> = [
    {
        id: ZodiacSign.ARIES,
        name: 'Овен',
        image: require('../../assets/icons/zodiacs/aries.png'),
        interval: {
            start: startOfDay(rangeDay('21-03')),
            end: endOfDay(rangeDay('20-04')),
        },
    },
    {
        id: ZodiacSign.TAURUS,
        name: 'Телец',
        image: require('../../assets/icons/zodiacs/taurus.png'),
        interval: {
            start: startOfDay(rangeDay('21-04')),
            end: endOfDay(rangeDay('21-05')),
        },
    },
    {
        id: ZodiacSign.GEMINI,
        name: 'Близнецы',
        image: require('../../assets/icons/zodiacs/gemini.png'),
        interval: {
            start: startOfDay(rangeDay('22-05')),
            end: endOfDay(rangeDay('21-06')),
        },
    },
    {
        id: ZodiacSign.CANCER,
        name: 'Рак',
        image: require('../../assets/icons/zodiacs/cancer.png'),
        interval: {
            start: startOfDay(rangeDay('22-06')),
            end: endOfDay(rangeDay('22-07')),
        },
    },
    {
        id: ZodiacSign.LEO,
        name: 'Лев',
        image: require('../../assets/icons/zodiacs/leo.png'),
        interval: {
            start: startOfDay(rangeDay('23-07')),
            end: endOfDay(rangeDay('21-08')),
        },
    },
    {
        id: ZodiacSign.VIRGO,
        name: 'Дева',
        image: require('../../assets/icons/zodiacs/virgo.png'),
        interval: {
            start: startOfDay(rangeDay('22-08')),
            end: endOfDay(rangeDay('23-09')),
        },
    },
    {
        id: ZodiacSign.LIBRA,
        name: 'Весы',
        image: require('../../assets/icons/zodiacs/libra.png'),
        interval: {
            start: startOfDay(rangeDay('24-09')),
            end: endOfDay(rangeDay('23-10')),
        },
    },
    {
        id: ZodiacSign.SCORPIO,
        name: 'Скорпион',
        image: require('../../assets/icons/zodiacs/scorpio.png'),
        interval: {
            start: startOfDay(rangeDay('24-10')),
            end: endOfDay(rangeDay('22-11')),
        },
    },
    {
        id: ZodiacSign.SAGITTARIUS,
        name: 'Стрелец',
        image: require('../../assets/icons/zodiacs/sagittarius.png'),
        interval: {
            start: startOfDay(rangeDay('23-11')),
            end: endOfDay(rangeDay('22-12')),
        },
    },
    {
        id: ZodiacSign.CAPRICORN,
        name: 'Козерог',
        image: require('../../assets/icons/zodiacs/capricorn.png'),
        interval: {
            start: startOfDay(parseWithOptions(
                {},
                new Date(),
                'dd-MM',
                '23-12',
            )),
            end: endOfDay(parseWithOptions(
                {},
                addYears(new Date(), 1),
                'dd-MM',
                '20-01',
            )),
        },
    },
    {
        id: ZodiacSign.AQUARIUS,
        name: 'Водолей',
        image: require('../../assets/icons/zodiacs/aquarius.png'),
        interval: {
            start: startOfDay(rangeDay('21-01')),
            end: endOfDay(rangeDay('19-02')),
        },
    },
    {
        id: ZodiacSign.PISCES,
        name: 'Рыбы',
        image: require('../../assets/icons/zodiacs/pisces.png'),
        interval: {
            start: startOfDay(rangeDay('20-02')),
            end: endOfDay(rangeDay('20-03')),
        },
    },
]


export const zodiacSignByDate = (date : Date) : Maybe<Zodiac> => Maybe
    .of(date)
    .chainNullable(v => zodiacMappings.find(zodiac => isWithinInterval(
        setYear(v, new Date().getFullYear()),
        zodiac.interval,
    )))
