import database from '@react-native-firebase/database'
import { Maybe, MaybeAsync } from 'purify-ts'
import { mapObjIndexed, pipe, prop, values } from 'ramda'

import { Dictionary } from 'interfaces'

import { FirebaseAuth } from './auth'


type FBContact = Dictionary<string>
type FBContactsResponse = Dictionary<FBContact>

export class FirebaseRealtimeDatabase {
    private readonly _auth : FirebaseAuth

    private _normalizedContacts = pipe(
        mapObjIndexed<FBContact, Dictionary<string>>((contact, key) => ({
            ...contact,
            id: key,
        })),
        values,
    )

    constructor(auth : FirebaseAuth) {
        this._auth = auth
    }

    private static async _fetchContacts(userId : string) : Promise<FBContactsResponse> {
        const snapshot = await database()
            .ref()
            .child('contacts')
            .orderByChild('userId')
            .equalTo(userId)
            .once('value')

        // database()
        //     .ref()
        //     .child('contacts')
        //     .push({
        //         userId,
        //         firstName: 'Яна',
        //         PhoneNumber: '+77435689032',
        //         lastName: 'Рязанова',
        //         middleName: 'Андреевна',
        //         tags: ['Семья'],
        //         birthDate: '28-08-1998',
        //     })

        return snapshot.val()
    }

    public async getContacts() : Promise<Array<Dictionary<string>>> {
        return MaybeAsync
            .liftMaybe(Maybe
                .fromNullable(this._auth.currentUser)
                .map(prop('uid')))
            .map(FirebaseRealtimeDatabase._fetchContacts)
            .map(this._normalizedContacts)
            .orDefault([])
    }
}
