import AsyncStorage from '@react-native-community/async-storage'
import { andThen, otherwise, pipe, T, F } from 'ramda'
import { Maybe } from 'purify-ts'

import { IContact } from 'interfaces'


type GetDataFunction = (key : string) => Promise<Maybe<any>>
type SetDataFunction = (key : string) => (value : string) => Promise<boolean>

export class Persist {
    private static _stringify(value : any) : string {
        return JSON.stringify(value)
    }

    private static _parse(value : string) : any {
        return JSON.parse(value)
    }

    public getContacts() : Promise<Maybe<Array<IContact>>> {
        return this._getDataByKey('contacts')
    }

    public setContacts(value : any) : Promise<boolean> {
        return this._setDataToKey('contacts')(value)
    }

    private _getValueFromStorage = (key : string) : Promise<string | null> => {
        return AsyncStorage.getItem(key)
    }

    private _setValueToStorage = (
        key : string,
    ) => (value : string) => AsyncStorage.setItem(key, value)

    // eslint-disable-next-line @typescript-eslint/member-ordering
    private _getDataByKey : GetDataFunction = pipe(
        this._getValueFromStorage,
        andThen(value => Maybe
            .fromNullable(value)
            .map(Persist._parse)),
        otherwise(Maybe.empty),
    )

    private _setDataToKey : SetDataFunction = key => pipe(
        Persist._stringify,
        this._setValueToStorage(key),
        andThen(T),
        otherwise(F),
    )
}
