import { AuthStoreModel } from '../auth-store/auth-store'
import { ContactsStoreModel } from '../contacts-store/contacts-store'
import { Environment } from '../environment'

import { RootStore, RootStoreModel } from './root-store'


export async function createEnvironment() : Promise<Environment> {
    const env = new Environment()

    await env.setup()

    return env
}

export async function setupRootStore() : Promise<RootStore> {
    const env = await createEnvironment()
    const maybePersistedContacts = await env.persist.getContacts()

    const rootStore : RootStore = RootStoreModel.create(
        {
            authStore: AuthStoreModel.create({
                user: env.auth.currentUser,
            }),
            contactsStore: ContactsStoreModel.create({
                items: maybePersistedContacts.orDefault([]),
                selectedTag: null,
                selectedItem: null,
            }),
        },
        env,
    )

    await rootStore.setup()

    return rootStore
}
