import { Instance, SnapshotOut, types, flow } from 'mobx-state-tree'

import { AuthStoreModel } from '../auth-store/auth-store'
import { ContactsStoreModel } from '../contacts-store/contacts-store'
import { withRootEnvironment } from '../extensions/with-root-environment'


export const RootStoreModel = types
    .model('RootStore')

    .props({
        authStore: AuthStoreModel,
        contactsStore: ContactsStoreModel,
    })

    .extend(withRootEnvironment)

    .actions(self => ({
        setup: flow(function* () {
            try {
                if (self.contactsStore.items.length === 0) {
                    yield self.contactsStore.fetchContacts()
                }
            }
            catch (e) {
                throw e
            }
        }),
    }))

export interface RootStore extends Instance<typeof RootStoreModel> {}

export interface RootStoreSnapshot extends SnapshotOut<typeof RootStoreModel> {}
