import {
    FirebaseAuth,
    FirebaseRealtimeDatabase,
    Persist,
} from 'services'


export class Environment {
    auth : FirebaseAuth

    database : FirebaseRealtimeDatabase

    persist : Persist

    constructor() {
        this.auth = new FirebaseAuth()
        this.persist = new Persist()
        this.database = new FirebaseRealtimeDatabase(this.auth)
    }

    public async setup() : Promise<void> {
        await this.auth.setup()
    }
}
