import { types, flow } from 'mobx-state-tree'

import { withRootEnvironment } from '../extensions/with-root-environment'


export const AuthStoreModel = types
    .model('AuthStore')

    .props({
        user: types.frozen(),
    })

    .extend(withRootEnvironment)

    .actions(self => ({
        signInAnonymously: flow(function* signInAnonymously() {
            yield self.environment.auth.signInAnonymously()
        }),
    }))

    .views(self => ({
        get isUserSignedIn() : boolean {
            return !!self.user
        },
    }))
