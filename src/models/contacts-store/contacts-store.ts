import { types, flow, getSnapshot } from 'mobx-state-tree'
import {
    map,
    defaultTo,
    flatten,
    pipe,
    prop,
    uniq,
    sortBy,
    toLower,
    filter,
    length,
    equals,
    ifElse,
    has,
    includes,
    isNil,
    propOr,
    F,
} from 'ramda'

import { currentMonthNumber, monthFromDatabaseDate } from 'helpers'

import { withStatus } from '../extensions/with-status'
import { withRootEnvironment } from '../extensions/with-root-environment'

import { IContactModel, ContactModel } from './contact'


export const ContactsStoreModel = types
    .model('ContactsStore')

    .props({
        items: types.array(ContactModel),

        selectedTag: types.maybeNull(
            types.string,
        ),

        selectedItem: types.maybeNull(
            types.reference(ContactModel),
        ),
    })

    .extend(withRootEnvironment)
    .extend(withStatus)

    .volatile(() => ({
        searchValue: '',
    }))

    .actions(self => ({
        setSearchValue: (value : string) => {
            self.searchValue = value
        },

        setSelectedTag(tag : string | null) {
            self.selectedTag = tag
        },

        setSelectedItem: (item : IContactModel | null) => {
            self.selectedItem = item
        },

        fetchContacts: flow(function* () {
            try {
                self.setStatus('pending')

                self.items = yield self.environment.database.getContacts()

                self.persistContacts()

                self.setStatus('done')

            }
            catch (e) {
                self.setStatus('error')

                throw e
            }
        }),

        persistContacts: () => {
            const items = getSnapshot(self.items)

            self.environment.persist.setContacts(items)
        },
    }))

    .views(self => ({
        get contactsByTag() : Array<IContactModel> {
            return ifElse(
                isNil,
                () => self.items,
                () => filter<IContactModel>(
                    pipe(
                        propOr([], 'tags'),
                        includes(self.selectedTag),
                    ),
                )(self.items),
            )(self.selectedTag)
        },

        get searchResult() : Array<IContactModel> {
            return filter<IContactModel>(
                pipe(
                    prop('searchableContent'),
                    includes(toLower(self.searchValue)),
                ),
            )(this.contactsByTag)
        },

        get searchResultCount() : number {
            return length(this.searchResult)
        },

        get birthdaysOfTheMonthCount() : number {
            return length(filter(
                ifElse(
                    has('parsedBirthDate'),
                    pipe(
                        prop('parsedBirthDate'),
                        monthFromDatabaseDate,
                        equals(currentMonthNumber),
                    ),
                    F,
                ),
                self.items,
            ))
        },

        get allTags() : Array<string> {
            return pipe(
                map(pipe(
                    prop('tags'),
                    defaultTo([]),
                )),
                flatten,
                uniq,
                sortBy(toLower),
            )(self.items)
        },
    }))
