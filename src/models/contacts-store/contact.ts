import { Instance, SnapshotIn, SnapshotOut, types } from 'mobx-state-tree'

import { databaseDate, formatPhoneNumber, parsedDatabaseDate } from 'helpers'


export const ContactModel = types
    .model('ContactModel')

    .props({
        id: types.identifier,
        firstName: types.string,
        phoneNumber: types.string,
        lastName: types.maybe(types.string),
        middleName: types.maybe(types.string),
        birthDate: types.maybe(types.string),
        remindDate: types.maybe(types.string),
        notes: types.maybe(types.string),
        tags: types.maybe(
            types.array(types.string),
        ),
    })

    .views(self => ({
        get searchableContent() : string {
            return `
                ${self.firstName}
                ${self.lastName || ''}
                ${self.middleName || ''}
                ${self.tags?.join('') || ''}
            `
                .trim()
                .toLowerCase()
        },

        get fullName() : string {
            return `${self.lastName || ''} ${self.firstName} ${self.middleName || ''}`
                .trim()
        },

        get internationalPhoneNumber() : string {
            return formatPhoneNumber(self.phoneNumber)
        },

        get uriPhoneNumber() : string {
            return formatPhoneNumber(self.phoneNumber, 'uri')
        },

        get parsedBirthDate() : Date | undefined {
            return self.birthDate !== undefined
                ? parsedDatabaseDate(self.birthDate)
                : undefined
        },

        get parsedRemindDate() : Date | undefined {
            return self.remindDate !== undefined
                ? parsedDatabaseDate(self.remindDate)
                : undefined
        },
    }))

    .actions(self => ({
        setBirthDate: (value : Date | undefined) => {
            self.birthDate = value !== undefined
                ? databaseDate(value)
                : undefined
        },

        setRemindDate: (value : Date | undefined) => {
            self.remindDate = value !== undefined
                ? databaseDate(value)
                : undefined
        },
    }))

export interface IContactModel extends Instance<typeof ContactModel> {}
export interface IContactSnapshotIn extends SnapshotIn<typeof ContactModel> {}
export interface IContactSnapshotOut extends SnapshotOut<typeof ContactModel> {}
