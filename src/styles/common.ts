import { Dimensions } from 'react-native'

import { palette } from './palette'


const {
    width: deviceWidth,
    height: deviceHeight,
} = Dimensions.get('window')

export const commonStyles = {
    deviceWidth,
    deviceHeight,

    mainAppBg: palette.alabaster,
    mainStatusBarColor: 'rgba(0, 0, 0, 0.4)',
    mainSidePadding: 15,

    mainButtonHeight: 52,
    mainButtonBorderRadius: 11,

    baseText: {
        color: palette.black,
        fontSize: 14,
        lineHeight: 17,
    },

    largeHeading: {
        color: palette.black,
        fontSize: 18,
        lineHeight: 22,
    },

    secondaryText: {
        color: palette.mantle,
    },

    linkColor: palette.azureRadiance,
    separatorColor: palette.selago,
    dangerColor: palette.bittersweet,

    modalBg: palette.white,
}
