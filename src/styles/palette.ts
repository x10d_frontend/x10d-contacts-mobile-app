// http://chir.ag/projects/name-that-color

export const palette = {
    black: '#000000',
    capeCod: '#292B2A',
    white: '#FFFFFF',
    alabaster: '#F9F9F9',
    selago: '#F5F5FD',
    porcelain: '#EDEEEF',
    silver: '#C7C7C7',
    manatee: '#98989D',
    mantle: '#939695',

    greenPea: '#20612E',
    azureRadiance: '#0073FA',

    bittersweet: '#FF6060',
    yellowOrange: '#FFB333',
    cornflowerBlue: '#7D7EF8',
    chateauGreen: '#35A14C',
    dodgerBlue: '#1FBCFF',
    amethyst: '#B557D6',
    flushMahogany: '#CD3E3E',
    pumpkin: '#FF7D1F',
    pastelGreen: '#80D657',

    transparent: 'transparent',
}
