import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import { ContactDetailsScreen } from 'screens'

import { ContactDetailsHeader, TabBarStackScreenHeader } from 'components'

import {
    CONTACT_DETAILS_SCREEN_NAME,
    TAB_BAR_STACK_NAME,
} from 'screen-names'

import { TabBarNavigator } from './tab-bar-navigator'


const Stack = createStackNavigator()

export const PrimaryNavigator : React.FC = () => {
    return (
        <Stack.Navigator
            headerMode="screen"
            initialRouteName={TAB_BAR_STACK_NAME}
        >
            <Stack.Screen
                name={TAB_BAR_STACK_NAME}
                component={TabBarNavigator}
                options={({ route }) => ({
                    header: () => <TabBarStackScreenHeader route={route} />,
                })}
            />
            <Stack.Screen
                name={CONTACT_DETAILS_SCREEN_NAME}
                component={ContactDetailsScreen}
                options={{
                    header: ({ navigation }) => (
                        <ContactDetailsHeader onBackPress={navigation.goBack} />
                    ),
                }}
            />
        </Stack.Navigator>
    )
}
