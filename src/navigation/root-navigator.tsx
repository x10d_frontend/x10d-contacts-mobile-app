import React from 'react'
import { NavigationContainer, NavigationContainerRef } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { observer } from 'mobx-react'

import { useStores } from 'models'

import { WELCOME_SCREEN_NAME } from 'screen-names'

import { WelcomeScreen } from 'screens'

import { PrimaryNavigator } from './primary-navigator'


export type RootParamList = {
    primaryStack : undefined
    [WELCOME_SCREEN_NAME] : undefined
}

const Stack = createStackNavigator<RootParamList>()

const RootStack = observer(() => {
    const { authStore } = useStores()

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
                gestureEnabled: true,
            }}
        >
            {authStore.isUserSignedIn
                ? (
                    <Stack.Screen
                        name="primaryStack"
                        component={PrimaryNavigator}
                    />
                )
                : (
                    <Stack.Screen
                        name={WELCOME_SCREEN_NAME}
                        component={WelcomeScreen}
                    />
                )
            }
        </Stack.Navigator>
    )
})

export const RootNavigator = React.forwardRef<
NavigationContainerRef,
Partial<React.ComponentProps<typeof NavigationContainer>>
>((props, ref) => {
    return (
        <NavigationContainer
            {...props}
            ref={ref}
        >
            <RootStack />
        </NavigationContainer>
    )
})

RootNavigator.displayName = 'RootNavigator'
