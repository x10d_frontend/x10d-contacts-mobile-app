import React from 'react'
import { Image, ImageSourcePropType } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import { ContactsScreen, TodayScreen } from 'screens'

import { palette } from 'styles'

import { Dictionary } from 'interfaces'

import { TabLabel } from 'components'

import { CONTACTS_SCREEN_NAME, TODAY_SCREEN_NAME } from 'screen-names'


const dateIcon = require('../../assets/icons/date.png')
const peopleIcon = require('../../assets/icons/people.png')



const BottomTab = createBottomTabNavigator()

export const TabBarNavigator : React.FC = () => (
    <BottomTab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ size, color }) => {
                const icons : Dictionary<ImageSourcePropType> = {
                    [TODAY_SCREEN_NAME]: dateIcon,
                    [CONTACTS_SCREEN_NAME]: peopleIcon,
                }

                return (
                    <Image
                        style={{
                            tintColor: color,
                            width: size,
                            height: size,
                        }}
                        source={icons[route.name]}
                        resizeMode="contain"
                    />
                )
            },
            tabBarLabel: ({ color }) => {
                const labels : Dictionary<string> = {
                    [TODAY_SCREEN_NAME]: 'Сегодня',
                    [CONTACTS_SCREEN_NAME]: 'Контакты',
                }

                return (
                    <TabLabel
                        title={labels[route.name]}
                        color={color}
                    />
                )
            },
        })}
        tabBarOptions={{
            activeTintColor: palette.chateauGreen,
            inactiveTintColor: palette.black,
            style: {
                height: 60,
                elevation: 9,
                borderTopColor: palette.white,
            },
        }}
    >
        <BottomTab.Screen
            name={TODAY_SCREEN_NAME}
            component={TodayScreen}
        />
        <BottomTab.Screen
            name={CONTACTS_SCREEN_NAME}
            component={ContactsScreen}
        />
    </BottomTab.Navigator>
)
