import React from 'react'
import {
    Image,
    ImageSourcePropType,
    StatusBar,
    View,
} from 'react-native'
import Swiper from 'react-native-swiper'

import { useStores } from 'models'

import {
    BaseAndroidNavigationBar,
    BaseButton,
    BaseText,
} from 'common-components'

import {
    commonStyles,
    palette,
} from 'styles'

import { styles } from './styles'



const confettiImage = require('../../../assets/images/welcome-confetti.png')
const ringImage = require('../../../assets/images/welcome-ring.png')

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface Props {

}

interface ISlideView {
    title : string

    image : ImageSourcePropType

    additionalContent : () => React.ReactNode
}

export const WelcomeScreen : React.FC<Props> = () => {
    const { authStore } = useStores()

    const handleFinishing = async () : Promise<void> => {
        await authStore.signInAnonymously()
    }

    const slides : Array<ISlideView> = [
        {
            title: 'Поздравляй друзей с праздниками!',
            image: confettiImage,
            additionalContent: () => (
                <BaseText
                    style={styles.subtitle}
                    medium
                >
                    {/* eslint-disable-next-line max-len */}
                    Теперь они не будут обижаться на то, что ты опять забыл поздравить c юбилеем их кошку
                </BaseText>
            ),
        },
        {
            title: 'Настраивай уведомления для важных событий',
            image: ringImage,
            additionalContent: () => (
                <BaseButton
                    style={styles.importContactsButton}
                    onPress={handleFinishing}
                >
                    <BaseText
                        style={styles.importContactsButton__text}
                        bold
                    >
                        Готово
                    </BaseText>
                </BaseButton>
            ),
        },
    ]

    return (
        <>
            <StatusBar
                backgroundColor={commonStyles.mainStatusBarColor}
                barStyle="light-content"
                animated
                translucent
            />

            <BaseAndroidNavigationBar
                backgroundColor={palette.white}
                darkButtons
            />

            <Swiper
                loop={false}
                dotStyle={styles.dot}
                activeDotStyle={styles.dot___active}
            >
                {slides.map(slide => (
                    <View
                        key={slide.title}
                        style={styles.container}
                    >
                        <Image
                            style={styles.image}
                            source={slide.image}
                        />

                        <BaseText
                            style={styles.title}
                            bold
                        >
                            {slide.title}
                        </BaseText>

                        {slide.additionalContent()}
                    </View>
                ))}
            </Swiper>
        </>
    )
}
