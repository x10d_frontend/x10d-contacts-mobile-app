import {
    StyleSheet,
    ViewStyle,
} from 'react-native'

import {
    commonStyles,
    palette,
} from 'styles'


const additionalContentWidth = commonStyles.deviceWidth * 0.8
const dotSize = 9
const baseDotStyle : ViewStyle = {
    width: dotSize,
    height: dotSize,
    marginRight: 7,
}

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: palette.white,
    },

    image: {
        width: '100%',
        height: commonStyles.deviceWidth * 0.8,
    },

    title: {
        maxWidth: commonStyles.deviceWidth * 0.7,
        marginTop: commonStyles.deviceWidth * 0.25,
        textAlign: 'center',
        fontSize: 24,
        lineHeight: 29,
    },

    subtitle: {
        maxWidth: additionalContentWidth,
        marginTop: commonStyles.deviceWidth * 0.08,
        textAlign: 'center',
    },

    importContactsButton: {
        width: additionalContentWidth,
        marginTop: commonStyles.deviceWidth * 0.14,
    },

    importContactsButton__text: {
        color: palette.white,
    },

    dot: {
        ...baseDotStyle,
        backgroundColor: palette.silver,
    },
    dot___active: {
        ...baseDotStyle,
        backgroundColor: palette.chateauGreen,
    },
})
