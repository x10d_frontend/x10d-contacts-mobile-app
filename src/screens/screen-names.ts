export const WELCOME_SCREEN_NAME = 'WelcomeScreen'

export const TAB_BAR_STACK_NAME = 'TabBarStack'
export const TODAY_SCREEN_NAME = 'TodayScreen'
export const CONTACTS_SCREEN_NAME = 'ContactsScreen'

export const CONTACT_DETAILS_SCREEN_NAME = 'ContactDetailsScreen'
