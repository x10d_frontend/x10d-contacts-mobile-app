import { StyleSheet } from 'react-native'

import { palette } from 'styles'


export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: palette.white,
    },
})
