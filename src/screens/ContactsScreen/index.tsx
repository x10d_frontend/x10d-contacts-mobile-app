import React from 'react'
import { StatusBar, View } from 'react-native'
import { observer } from 'mobx-react'

import { BaseAndroidNavigationBar, BaseFloatingActionButton } from 'common-components'

import { ContactsSearchInput, ContactsTabs } from 'components'

import {
    commonStyles,
    palette,
} from 'styles'

import { styles } from './styles'


export const ContactsScreen : React.FC = observer(() => {
    return (
        <>
            <StatusBar
                backgroundColor={commonStyles.mainStatusBarColor}
                barStyle="light-content"
                animated
                translucent
            />

            <BaseAndroidNavigationBar
                backgroundColor={palette.white}
                darkButtons
            />

            <View style={styles.container}>
                <ContactsSearchInput />

                <ContactsTabs />

                <BaseFloatingActionButton />
            </View>
        </>
    )
})
