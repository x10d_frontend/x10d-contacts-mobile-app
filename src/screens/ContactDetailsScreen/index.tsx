import React from 'react'
import {
    Image,
    Linking,
    ScrollView,
    StatusBar,
    TouchableOpacity,
    View,
} from 'react-native'
import { observer } from 'mobx-react'

import { BaseAndroidNavigationBar, BaseModal, BaseText } from 'common-components'

import { useStores } from 'models'

import { commonStyles, palette } from 'styles'

import { ContactDetailsBirthdayPanel } from '../../components/ContactDetailsBirthdayPanel'

import { styles } from './styles'

const stubImage = require('../../../assets/images/contact-details-stub.png')



interface ISection {
    name : string

    component : React.FC<any>

    componentProps : any

    visibilityCondition : () => boolean
}


export const ContactDetailsScreen : React.FC = observer(() => {
    const { contactsStore } = useStores()
    const { selectedItem } = contactsStore

    if (selectedItem === null) {
        return null
    }

    const sections : Array<ISection> = [
        {
            name: 'birthDate',
            component: ContactDetailsBirthdayPanel,
            componentProps: {
                birthDate: selectedItem.parsedBirthDate,
                remindDate: selectedItem.parsedRemindDate,
                setRemindDate: selectedItem.setRemindDate,
            },
            visibilityCondition: () => selectedItem.parsedBirthDate !== undefined,
        },
    ]

    const launchCallIntent = async () : Promise<void> => {
        await Linking.openURL(selectedItem.uriPhoneNumber)
    }

    return (
        <>
            <StatusBar
                backgroundColor={palette.greenPea}
                barStyle="light-content"
                animated
                translucent
            />

            <BaseAndroidNavigationBar
                backgroundColor={BaseModal.modalParamsStack.length !== 0
                    ? commonStyles.modalBg
                    : commonStyles.mainAppBg
                }
                darkButtons
            />

            <ScrollView
                style={styles.container}
                contentContainerStyle={styles.contentContainer}
            >
                <View style={styles.headerContinuation} />

                <Image
                    style={styles.stubImage}
                    source={stubImage}
                />

                <BaseText
                    bold
                    style={styles.fullName}
                >
                    {selectedItem.fullName}
                </BaseText>

                <TouchableOpacity
                    onPress={launchCallIntent}
                    activeOpacity={0.8}
                    hitSlop={{
                        top: 10,
                        right: 10,
                        bottom: 10,
                        left: 10,
                    }}
                >
                    <BaseText
                        medium
                        selectable
                        style={styles.phoneNumber}
                    >
                        {selectedItem.internationalPhoneNumber}
                    </BaseText>
                </TouchableOpacity>

                {sections.map(section => (
                    section.visibilityCondition()
                        ? (
                            <View
                                key={section.name}
                                style={styles.section}
                            >
                                <section.component {...section.componentProps} />
                            </View>
                        )
                        : null
                ))}
            </ScrollView>
        </>
    )
})
