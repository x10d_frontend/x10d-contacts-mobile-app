import { StyleSheet } from 'react-native'

import {
    commonStyles,
    palette,
} from 'styles'


export const stubSize = commonStyles.deviceWidth * 0.26

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        zIndex: 1000,
        backgroundColor: commonStyles.mainAppBg,
    },

    contentContainer: {
        alignItems: 'center',
        paddingHorizontal: commonStyles.mainSidePadding,
    },

    headerContinuation: {
        height: stubSize / 2,
        width: commonStyles.deviceWidth,
        backgroundColor: palette.chateauGreen,
        borderBottomLeftRadius: 22,
        borderBottomRightRadius: 22,
    },

    stubImage: {
        width: stubSize,
        height: stubSize,
        marginTop: -stubSize / 2,
    },

    fullName: {
        textAlign: 'center',
        maxWidth: commonStyles.deviceWidth * 0.5,
        marginTop: 10,
        fontSize: 15,
        lineHeight: 18,
        color: palette.capeCod,
    },

    phoneNumber: {
        textAlign: 'center',
        maxWidth: commonStyles.deviceWidth * 0.5,
        marginTop: 5,
        marginBottom: 8,
        color: palette.manatee,
    },

    section: {
        width: '100%',
        alignItems: 'center',
        marginTop: 10,
        paddingVertical: 12,
        borderRadius: commonStyles.mainButtonBorderRadius,
        backgroundColor: palette.white,
        elevation: 20,
    },
})
