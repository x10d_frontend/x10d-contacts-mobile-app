import { StyleSheet } from 'react-native'

import { commonStyles, palette } from 'styles'


export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: palette.alabaster,
        paddingHorizontal: commonStyles.mainSidePadding,
    },
})
