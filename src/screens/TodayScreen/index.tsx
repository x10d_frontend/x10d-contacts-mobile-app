import React from 'react'
import { StatusBar, View } from 'react-native'

import { BaseAndroidNavigationBar } from 'common-components'

import { TodayScreenBirthdays, TodayScreenWeek } from 'components'

import { commonStyles, palette } from 'styles'

import { styles } from './styles'


export const TodayScreen : React.FC = () => {
    return (
        <View style={styles.container}>
            <StatusBar
                backgroundColor={commonStyles.mainStatusBarColor}
                barStyle="light-content"
                animated
                translucent
            />

            <BaseAndroidNavigationBar
                backgroundColor={palette.white}
                darkButtons
            />

            <TodayScreenWeek />

            <TodayScreenBirthdays />
        </View>
    )
}
