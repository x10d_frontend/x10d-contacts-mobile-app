export * from './common'

export type { IContactSnapshotOut as IContact } from '../models/contacts-store/contact'
