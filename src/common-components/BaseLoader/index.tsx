import React from 'react'
import { WaveIndicator } from 'react-native-indicators'

import { palette } from 'styles'


export const BaseLoader : React.FC = () => (
    <WaveIndicator
        color={palette.chateauGreen}
    />
)
