import React, { createRef } from 'react'
import { Modalize, ModalizeProps } from 'react-native-modalize'
import { action, flow, observable } from 'mobx'

import { delay, randomId } from 'helpers'



export type ModalShowParams = ModalizeProps

interface ModalParams extends ModalShowParams {
    id : number

    ref : React.RefObject<Modalize>

    // Анимированно закрыть модальное окно
    // и удалить его из стэка.
    closeModal : () => Promise<void>

    // Удалить модальное окно из стэка.
    // Используется, например, при закрытии
    // окна жестом.
    removeModalFromStack : () => Promise<void>
}


class BaseModalService {
    modalParamsStack = observable.array<ModalParams>([])

    public show = action((
        getOptions : (closeModal : ModalParams['closeModal']) => ModalShowParams,
    ) : Promise<void> => {

        const modalId = randomId()

        return new Promise((resolve) => {
            const closeModal = () : Promise<void> => this.close(modalId, resolve)
            const removeModalFromStack = () : Promise<void> => this.close(
                modalId,
                resolve,
                true,
            )

            this.modalParamsStack.push({
                ...getOptions(closeModal),
                id: modalId,
                ref: createRef<Modalize>(),
                closeModal,
                removeModalFromStack,
            })
        })

    })

    public close = flow(function* (
        this : BaseModalService,
        modalId : number,
        showModalPromiseResolver : () => void,
        withoutAnimation ?: boolean,
    ) {

        const closingModal = this.modalParamsStack.find(
            e => e.id === modalId,
        )

        if (closingModal !== undefined) {
            if (withoutAnimation !== true) {
                // eslint-disable-next-line no-unused-expressions
                closingModal.ref.current?.close()

                // https://jeremybarbet.github.io/react-native-modalize/#/PROPS?id=openanimationconfig
                yield delay(closingModal.closeAnimationConfig?.timing.duration || 280)
            }

            this.modalParamsStack.remove(closingModal)
        }

        showModalPromiseResolver()

    })
}

export const BaseModal = new BaseModalService()
