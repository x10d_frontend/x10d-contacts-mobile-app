import React, { useEffect } from 'react'
import { Modalize } from 'react-native-modalize'
import { observer } from 'mobx-react'

import { BaseModal } from './service'

import { styles } from './styles'


const BaseModalProvider : React.FC = observer(() => {
    useEffect(() => {
        BaseModal.modalParamsStack.forEach((modalParams) => {
            // eslint-disable-next-line no-unused-expressions
            modalParams.ref.current?.open()
        })
    })

    return (
        <>
            {BaseModal.modalParamsStack.map((params) => {
                const {
                    id,
                    ref,
                    removeModalFromStack,
                    children,
                    ...restParams
                } = params

                return (
                    <Modalize
                        key={id}
                        ref={ref}
                        onClosed={removeModalFromStack}
                        modalStyle={styles.modal}
                        adjustToContentHeight
                        {...restParams}
                    >
                        {children}
                    </Modalize>
                )
            })}
        </>
    )
})

export {
    BaseModal,
    BaseModalProvider,
}
