import { StyleSheet } from 'react-native'

import { commonStyles } from 'styles'


export const styles = StyleSheet.create({
    modal: {
        backgroundColor: commonStyles.modalBg,
        borderTopLeftRadius: 19,
        borderTopRightRadius: 19,
    },
})
