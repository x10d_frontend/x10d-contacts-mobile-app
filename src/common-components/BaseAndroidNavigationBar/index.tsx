import React from 'react'
import { Platform } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'
import changeNavigationBarColor from 'react-native-navigation-bar-color'


function changeColorsByProps(props : Props) : void {
    changeNavigationBarColor(
        props.backgroundColor,
        props.darkButtons,
        true,
    )
}

interface Props {
    backgroundColor : string

    darkButtons : boolean
}

export const BaseAndroidNavigationBar : React.FC<Props> = (props : Props) => {
    if (Platform.OS === 'android') {
        useFocusEffect(
            React.useCallback(() => {
                changeColorsByProps(props)

            }, [props]),
        )
    }

    return null
}
