import { StyleSheet } from 'react-native'

import {
    commonStyles,
    palette,
} from 'styles'


export const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        height: commonStyles.mainButtonHeight,
        backgroundColor: palette.chateauGreen,
        borderRadius: commonStyles.mainButtonBorderRadius,
    },
})
