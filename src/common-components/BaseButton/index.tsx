import React from 'react'
import {
    TouchableOpacity,
    TouchableOpacityProps,
} from 'react-native'

import { styles } from './styles'



type Props = TouchableOpacityProps

export const BaseButton : React.FC<Props> = (props : Props) => {
    const {
        style,
        ...restProps
    } = props

    return (
        <TouchableOpacity
            style={[
                styles.container,
                style,
            ]}
            activeOpacity={0.8}
            hitSlop={{
                top: 10,
                right: 10,
                bottom: 10,
                left: 10,
            }}
            accessibilityRole="button"
            {...restProps}
        />
    )
}
