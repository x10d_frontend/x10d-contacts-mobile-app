import { StyleSheet } from 'react-native'

import { commonStyles, palette } from 'styles'


export const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        right: commonStyles.mainSidePadding * 2,
        bottom: commonStyles.mainSidePadding,
        justifyContent: 'center',
        alignItems: 'center',
        width: 40,
        height: 40,
        backgroundColor: palette.chateauGreen,
        borderRadius: 9,
        elevation: 7,
    },

    image: {
        width: 17,
        height: 17,
        tintColor: palette.white,
    },
})
