import React from 'react'
import {
    Image,
    TouchableOpacity,
    TouchableOpacityProps,
} from 'react-native'

import { styles } from './styles'

const plusImage = require('../../../assets/icons/plus.png')



type Props = TouchableOpacityProps

export const BaseFloatingActionButton : React.FC<Props> = (props : Props) => {
    const { style, ...restProps } = props

    return (
        <TouchableOpacity
            style={[
                styles.container,
                style,
            ]}
            activeOpacity={0.8}
            {...restProps}
        >
            <Image
                style={styles.image}
                source={plusImage}
            />
        </TouchableOpacity>
    )
}
