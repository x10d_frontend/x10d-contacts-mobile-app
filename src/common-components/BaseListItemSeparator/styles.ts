import { StyleSheet } from 'react-native'

import { palette } from 'styles'


export const styles = StyleSheet.create({
    container: {
        height: 1,
        backgroundColor: palette.selago,
    },
})
