import React from 'react'
import { View } from 'react-native'

import { styles } from './styles'


export const BaseListItemSeparator : React.FC = () => (
    <View style={styles.container} />
)
