import React from 'react'
import { Text, TextProps } from 'react-native'

import { Dictionary } from 'interfaces'

import { commonStyles } from 'styles'


interface Props extends TextProps {
    medium ?: boolean
    bold ?: boolean
}

const fontFamilyMapping : Dictionary<string> = {
    regular: 'Montserrat-Regular',
    medium: 'Montserrat-Medium',
    bold: 'Montserrat-Bold',
}

export const BaseText : React.FC<Props> = (props : Props) => {
    const {
        style,
        bold,
        medium,
        ...restProps
    } = props

    let weight = 'regular'

    if (medium === true) weight = 'medium'
    if (bold === true) weight = 'bold'

    return (
        <Text
            style={[
                { fontFamily: fontFamilyMapping[weight] },
                commonStyles.baseText,
                style,
            ]}
            accessibilityRole="text"
            {...restProps}
        />
    )
}
