import React from 'react'
import { ViewStyle } from 'react-native'
// @ts-ignore Нет типов
import Switch from 'react-native-switch-pro'
import { observer } from 'mobx-react'

import { palette } from 'styles'

import { styles } from './styles'


interface Props {
    value : boolean

    onChange ?: () => void

    disabled ?: boolean

    style ?: ViewStyle
}

export const BaseSwitch : React.FC<Props> = observer((props : Props) => {
    return (
        <Switch
            style={[
                styles.container,
                props.style,
            ]}
            circleStyle={styles.circleStyle}
            value={props.value}
            disabled={props.disabled}
            width={60}
            height={34}
            circleColorActive={palette.white}
            backgroundActive={palette.chateauGreen}
            circleColorInactive={palette.porcelain}
            backgroundInactive={palette.white}
            pointerEvents={props.disabled ? 'none' : 'auto'}
        />
    )
})
