import { StyleSheet } from 'react-native'

import { palette } from 'styles'


export const styles = StyleSheet.create({
    container: {
        padding: 4,
        borderWidth: 1,
        borderColor: palette.porcelain,
    },

    circleStyle: {
        width: 26,
        height: 26,
    },
})
