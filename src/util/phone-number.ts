import { Maybe, Just } from 'purify-ts/Maybe'
import { PhoneNumber as PN, parsePhoneNumberFromString } from 'libphonenumber-js'


export function formatPhoneNumber(
    value : string,
    formatType : 'international' | 'uri' = 'international',
) : string {
    return Maybe
        .fromNullable<PN>(parsePhoneNumberFromString(value, 'RU'))
        .chain(phoneNumber => Just(
            formatType === 'international'
                ? phoneNumber.formatInternational()
                : phoneNumber.getURI(),
        ))
        .orDefault(value)
}
