import { pipe } from 'ramda'
import { formatWithOptions, formatDistanceStrictWithOptions, parseISO } from 'date-fns/fp'
import { eachDayOfInterval, endOfWeek, getMonth, startOfWeek } from 'date-fns'
import { ru } from 'date-fns/locale'


// Общие функции
export const dayOfMonth = formatWithOptions({ locale: ru }, 'd MMMM')
export const distanceToNow = formatDistanceStrictWithOptions(
    { locale: ru, unit: 'year', roundingMethod: 'ceil' },
    new Date(),
)

export const currentMonthName = formatWithOptions({ locale: ru }, 'LLLL', new Date())
export const currentMonthNumber = getMonth(new Date())
export const dayOfWeek = formatWithOptions({ locale: ru }, 'EEEEEE')
export const databaseDate = formatWithOptions({ locale: ru }, 'yyyy-MM-dd')

export const currentWeekDays = eachDayOfInterval({
    start: startOfWeek(new Date(), { locale: ru, weekStartsOn: 1 }),
    end: endOfWeek(new Date(), { locale: ru, weekStartsOn: 1 }),
})

export const parsedDatabaseDate = parseISO
export const monthFromDatabaseDate = pipe(parsedDatabaseDate, getMonth)


/**
 * Функция, возвращающая дату дня Рождения в формате дня месяца
 *
 * @example
 * birthday(02-11-1993) // 2 ноября
 */
export const birthday = dayOfMonth
