import { SectionListData } from 'react-native'
import {
    compose,
    groupBy,
    mapObjIndexed,
    prop,
    sortBy,
    values,
} from 'ramda'


interface TitledSectionListData<T> extends SectionListData<T> {
    title : string
}

/**
 * Функция, помогающая в формировании структуры данных для ReactNative SectionList.
 *
 * @param getSectionTitle - Функция, возвращающая заголовок секции, по которому
 * происходит группировка элементов
 *
 * @returns<Function> - Функция, которая принимает в себя массив элементов и возвращает
 * необходимую для SectionList структуру, отсортированную по заголовку секции
 */
export function getSectionListData<T>(
    getSectionTitle : (item : T) => string,
) : (payload : Array<T>) => Array<SectionListData<T>> {
    return compose(
        // @ts-ignore Бяка с типами
        sortBy(prop('title')),
        values,
        mapObjIndexed<Array<T>, TitledSectionListData<T>>((value, key) => ({
            title: key,
            data: value,
        })),
        groupBy<T>(getSectionTitle),
    )
}


export const throws = (message : string) => () => {
    throw new Error(message)
}


export const randomId = () : number => Math.round(
    Math.random() * 1000000,
)

type DelayFunction = (ms : number) => Promise<void>
export const delay : DelayFunction = ms => new Promise(
    resolve => setTimeout(resolve, ms),
)

export * from './date'
export * from './phone-number'
export * from './tag-colors'
