import { __, pipe, modulo, nth, length, defaultTo } from 'ramda'

import { palette } from 'styles'


const tagColors = [
    palette.bittersweet,
    palette.yellowOrange,
    palette.cornflowerBlue,
    palette.chateauGreen,
    palette.dodgerBlue,
    palette.amethyst,
    palette.flushMahogany,
    palette.pumpkin,
    palette.pastelGreen,
]

export const tagColor = pipe(
    modulo(__, length(tagColors)),
    // @ts-ignore
    nth(__, tagColors),
    defaultTo(palette.bittersweet),
)
