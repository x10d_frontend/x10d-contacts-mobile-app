import React from 'react'
import { TouchableOpacity } from 'react-native'

import { BaseText } from 'common-components'

import { BottomModalPanelHeader } from '../BottomModalPanelHeader'

import { styles } from './styles'


interface Props {
    items : Array<IPickerItem>

    onSelect : (item : IPickerItem) => void

    closeModal : () => void
}

export interface IPickerItem {
    value : Date | undefined

    title : string
}


export const ContactDetailsRemindDatePicker : React.FC<Props> = (props) => {
    return (
        <>
            <BottomModalPanelHeader
                title="Напоминание"
                closeModal={props.closeModal}
            />

            {props.items.map(item => (
                <TouchableOpacity
                    key={item.title}
                    style={styles.item}
                    onPress={() => props.onSelect(item)}
                    activeOpacity={0.8}
                >
                    <BaseText
                        medium
                        style={[
                            styles.itemText,
                            item.value === undefined
                                ? styles.itemText__destructive
                                : null,
                        ]}
                    >
                        {item.title}
                    </BaseText>
                </TouchableOpacity>
            ))}
        </>
    )
}
