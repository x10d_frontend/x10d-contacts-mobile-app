import { StyleSheet } from 'react-native'

import { commonStyles, palette } from 'styles'


export const styles = StyleSheet.create({
    item: {
        alignItems: 'center',
        paddingVertical: 14,
        borderTopColor: commonStyles.separatorColor,
        borderTopWidth: 1,
    },

    itemText: commonStyles.baseText,

    itemText__destructive: {
        color: commonStyles.dangerColor,
    },
})
