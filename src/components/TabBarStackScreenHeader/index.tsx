import React from 'react'
import { View, ViewStyle, StyleProp } from 'react-native'

import { Dictionary } from 'interfaces'

import { CONTACTS_SCREEN_NAME, TODAY_SCREEN_NAME } from 'screen-names'

import { TodayScreenHeader } from '../TodayScreenHeader'
import { ContactsScreenHeader } from '../ContactsScreenHeader'

import { styles } from './styles'


interface IScreenHeaderProps {
    route : any
}

interface IContentMappings {
    content : React.FC

    contentContainerStyle : StyleProp<ViewStyle>
}

export const TabBarStackScreenHeader : React.FC<IScreenHeaderProps> =
    (props : IScreenHeaderProps) => {
        const { route } = props

        const contentMappings : Dictionary<IContentMappings> = {
            [TODAY_SCREEN_NAME]: {
                content: TodayScreenHeader,
                contentContainerStyle: styles.container__today,
            },
            [CONTACTS_SCREEN_NAME]: {
                content: ContactsScreenHeader,
                contentContainerStyle: styles.container__contacts,
            },
        }

        const currentRouteName = route.state
            ? route.state.routes[route.state.index].name
            : route.params?.screen || TODAY_SCREEN_NAME

        const {
            content: Content,
            contentContainerStyle,
        } = contentMappings[currentRouteName]

        return (
            <View style={[styles.container, contentContainerStyle]}>
                <Content />
            </View>
        )
    }
