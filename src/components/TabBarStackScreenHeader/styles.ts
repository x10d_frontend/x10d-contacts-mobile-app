import {
    StatusBar,
    StyleSheet,
} from 'react-native'

import {
    commonStyles,
    palette,
} from 'styles'


export const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 50,
        marginTop: StatusBar.currentHeight,
        paddingHorizontal: commonStyles.mainSidePadding,
    },

    container__contacts: {
        backgroundColor: palette.white,
        borderBottomWidth: 1,
        borderBottomColor: palette.selago,
    },

    container__today: {
        paddingHorizontal: 18,
        backgroundColor: palette.alabaster,
    },
})
