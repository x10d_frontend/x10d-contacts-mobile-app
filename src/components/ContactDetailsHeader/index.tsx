import React from 'react'
import {
    Image,
    TouchableOpacity,
    View,
} from 'react-native'

import { styles } from './styles'

const backImage = require('../../../assets/icons/back.png')
const ellipsisImage = require('../../../assets/icons/ellipsis.png')


interface IProps {
    onBackPress : () => void
}

export const ContactDetailsHeader : React.FC<IProps> = props => (
    <View style={styles.container}>
        <TouchableOpacity
            onPress={props.onBackPress}
            activeOpacity={0.8}
            hitSlop={{
                top: 10,
                right: 10,
                bottom: 10,
                left: 10,
            }}
        >
            <Image
                source={backImage}
                style={styles.backImage}
            />
        </TouchableOpacity>

        <TouchableOpacity
            activeOpacity={0.8}
            hitSlop={{
                top: 10,
                right: 10,
                bottom: 10,
                left: 10,
            }}
        >
            <Image
                source={ellipsisImage}
                style={styles.ellipsisImage}
            />
        </TouchableOpacity>
    </View>
)
