import {
    StatusBar,
    StyleSheet,
} from 'react-native'

import {
    commonStyles,
    palette,
} from 'styles'


export const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: commonStyles.deviceWidth * 0.14,
        marginTop: StatusBar.currentHeight,
        paddingHorizontal: 26,
        backgroundColor: palette.chateauGreen,
    },

    backImage: {
        width: 10,
        height: 18,
    },

    ellipsisImage: {
        width: 22,
        height: 5,
    },
})
