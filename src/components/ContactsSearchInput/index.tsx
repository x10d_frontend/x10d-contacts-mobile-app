import React from 'react'
import { Image, TouchableOpacity, TextInput, View } from 'react-native'
import { observer } from 'mobx-react'

import { useStores } from 'models'

import { palette } from 'styles'

import { styles } from './styles'

const searchImage = require('../../../assets/icons/search.png')
const crossImage = require('../../../assets/icons/cross.png')


export const ContactsSearchInput : React.FC = observer(() => {
    const { contactsStore } = useStores()

    const resetSearchValue = () : void => {
        contactsStore.setSearchValue('')
    }

    return (
        <View style={styles.container}>
            <Image
                style={styles.searchImage}
                source={searchImage}
            />

            <TextInput
                style={styles.input}
                value={contactsStore.searchValue}
                onChangeText={contactsStore.setSearchValue}
                placeholder="Поиск контактов"
                placeholderTextColor={palette.manatee}
                selectionColor={palette.chateauGreen}
                returnKeyType="search"
                autoCorrect={false}
            />

            {contactsStore.searchValue !== '' && (
                <TouchableOpacity
                    activeOpacity={0.8}
                    hitSlop={{
                        top: 10,
                        right: 10,
                        bottom: 10,
                        left: 10,
                    }}
                    onPress={resetSearchValue}
                >
                    <Image
                        style={styles.crossImage}
                        source={crossImage}
                    />
                </TouchableOpacity>
            )}
        </View>
    )
})
