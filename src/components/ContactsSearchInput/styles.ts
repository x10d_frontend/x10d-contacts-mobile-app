import { StyleSheet } from 'react-native'

import {
    commonStyles,
    palette,
} from 'styles'


export const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
        margin: commonStyles.mainSidePadding,
        paddingVertical: commonStyles.mainSidePadding * 2 / 3,
        paddingHorizontal: commonStyles.mainSidePadding,
        backgroundColor: palette.selago,
        borderRadius: 2,
    },

    searchImage: {
        width: 18,
        height: 18,
        tintColor: palette.manatee,
    },

    input: {
        flex: 1,
        marginHorizontal: 20,
        padding: 0,
        color: palette.manatee,
        fontSize: 14,
        lineHeight: 24,
        fontFamily: 'Montserrat-Regular',
    },

    crossImage: {
        width: 10,
        height: 10,
        tintColor: palette.manatee,
    },
})
