import { StyleSheet } from 'react-native'

import { commonStyles, palette } from 'styles'


export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: palette.white,
    },

    stubText: {
        ...commonStyles.largeHeading,
        alignSelf: 'center',
        marginTop: commonStyles.deviceHeight * 0.25,
        color: palette.mantle,
    },
})
