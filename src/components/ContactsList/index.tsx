import React from 'react'
import { SectionList, SectionListRenderItem } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { observer } from 'mobx-react'

import { useStores } from 'models'

import { BaseListItemSeparator, BaseText } from 'common-components'

import { ContactsListItem } from 'components/ContactsListItem'
import { ContactsListSectionHeader } from 'components/ContactsListSectionHeader'

import { IContactModel } from 'models/contacts-store/contact'

import { getSectionListData } from 'helpers'

import { CONTACT_DETAILS_SCREEN_NAME } from 'screen-names'

import { styles } from './styles'



const getGroupedContacts = getSectionListData<IContactModel>((item) => {
    if (item.lastName !== undefined) return item.lastName[0].toLowerCase()

    return item.firstName[0].toLowerCase()
})


export const ContactsList : React.FC = observer(() => {
    const { contactsStore } = useStores()
    const navigation = useNavigation()

    const sections = getGroupedContacts(contactsStore.searchResult)

    const onPress = (item : IContactModel) : void => {
        contactsStore.setSelectedItem(item)

        navigation.navigate(CONTACT_DETAILS_SCREEN_NAME)
    }

    const renderItem : SectionListRenderItem<IContactModel> = ({ item }) => (
        <ContactsListItem
            item={item}
            onPress={onPress}
        />
    )

    const ListEmptyComponent : React.FC = () => (
        <BaseText
            style={styles.stubText}
            bold
        >
            {
                contactsStore.searchValue.length !== 0
                    ? 'Никого не нашли'
                    : 'Список пуст'
            }
        </BaseText>
    )

    return (
        <SectionList<IContactModel>
            sections={sections}
            keyExtractor={item => item.id}
            renderItem={renderItem}
            ItemSeparatorComponent={BaseListItemSeparator}
            ListEmptyComponent={ListEmptyComponent}
            renderSectionHeader={ContactsListSectionHeader}
            initialNumToRender={20}
            keyboardDismissMode="on-drag"
            keyboardShouldPersistTaps="handled"
            stickySectionHeadersEnabled
        />
    )
})
