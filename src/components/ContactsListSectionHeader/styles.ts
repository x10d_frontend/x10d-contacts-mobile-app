import { StyleSheet } from 'react-native'

import { palette } from 'styles'


export const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        height: 28,
        paddingLeft: 22,
        backgroundColor: palette.selago,
    },

    text: {
        textTransform: 'uppercase',
    },
})
