import React from 'react'
import { SectionListProps, View } from 'react-native'

import { BaseText } from 'common-components'

import { IContactModel } from 'models/contacts-store/contact'

import { styles } from './styles'


export const ContactsListSectionHeader : SectionListProps<IContactModel>['renderSectionHeader'] =
    (info) => {
        return (
            <View style={styles.container}>
                <BaseText
                    bold
                    style={styles.text}
                >
                    {info.section.title}
                </BaseText>
            </View>
        )
    }
