import { StyleSheet } from 'react-native'

import { palette } from 'styles'


export const styles = StyleSheet.create({
    tabBarContainer: {
        paddingHorizontal: 10,
    },

    tabBar: {
        height: 40,
        backgroundColor: palette.white,
    },

    tabBarItem: {
        width: 'auto',
    },

    tabBarIndicatorContainer: {
        marginHorizontal: 10,
    },

    tabBarIndicator: {
        height: 3,
        backgroundColor: palette.yellowOrange,
        borderTopRightRadius: 2,
        borderTopLeftRadius: 2,
    },

    tabBarItemLabel: {
        marginBottom: 10,
    },
})
