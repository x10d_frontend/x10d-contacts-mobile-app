import React, { useState } from 'react'
import { TabView, TabBar, TabBarProps, TabViewProps } from 'react-native-tab-view'
import { prepend } from 'ramda'

import { BaseText, BaseLoader } from 'common-components'

import { useStores } from 'models'

import { commonStyles } from 'styles'

import { ContactsList } from '../ContactsList'

import { styles } from './styles'



interface ITab {
    key : string

    title : string
}


export const ContactsTabs : React.FC = () => {
    const { contactsStore } = useStores()
    const [activeTabIndex, setActiveTabIndex] = useState(0)

    const allTags = prepend('Все', contactsStore.allTags)

    const routes : Array<ITab> = allTags.map(tag => ({
        key: tag,
        title: tag,
        accessibilityLabel: tag,
    }))

    const renderScene : TabViewProps<ITab>['renderScene'] = () => <ContactsList />

    const renderTabBarLabel : TabBarProps<ITab>['renderLabel'] = ({ route, focused }) => {
        // https://github.com/satya164/react-native-tab-view/issues/1004
        const title = route.title.length > 10
            ? `${route.title}  `
            : `${route.title} `

        return (
            <BaseText
                style={styles.tabBarItemLabel}
                bold={focused}
                medium={!focused}
            >
                {title}
            </BaseText>
        )
    }

    const renderTabBar : TabViewProps<ITab>['renderTabBar'] = props => (
        <TabBar
            {...props}
            style={styles.tabBar}
            contentContainerStyle={styles.tabBarContainer}
            tabStyle={styles.tabBarItem}
            indicatorStyle={styles.tabBarIndicator}
            indicatorContainerStyle={styles.tabBarIndicatorContainer}
            renderLabel={renderTabBarLabel}
            scrollEnabled
        />
    )

    const renderLazyPlaceholder = () : React.ReactNode => <BaseLoader />

    const handleTabChanging = (index : number) : void => {
        const valueToSet = index !== 0
            ? routes[index].key
            : null

        contactsStore.setSelectedTag(valueToSet)

        setActiveTabIndex(index)
    }

    return (
        <TabView
            navigationState={{
                index: activeTabIndex,
                routes,
            }}
            renderTabBar={renderTabBar}
            renderLazyPlaceholder={renderLazyPlaceholder}
            renderScene={renderScene}
            onIndexChange={handleTabChanging}
            lazy
            removeClippedSubviews
            lazyPreloadDistance={1}
            initialLayout={{
                width: commonStyles.deviceWidth,
                height: 0,
            }}
        />
    )
}
