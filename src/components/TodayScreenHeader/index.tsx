import React from 'react'

import { BaseText } from 'common-components'

import { currentMonthName } from 'helpers'

import { styles } from './styles'


export const TodayScreenHeader : React.FC = () => {
    return (
        <>
            <BaseText
                style={styles.title}
                bold
            >
                {currentMonthName}
            </BaseText>
        </>
    )
}
