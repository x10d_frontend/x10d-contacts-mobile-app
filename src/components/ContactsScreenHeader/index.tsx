import React from 'react'
import { observer } from 'mobx-react'

import { BaseText } from 'common-components'

import { useStores } from 'models'

import { styles } from './styles'


export const ContactsScreenHeader : React.FC = observer(() => {
    const { contactsStore } = useStores()

    return (
        <>
            <BaseText
                style={styles.title}
                bold
            >
                Контакты
            </BaseText>

            <BaseText
                style={styles.subTitle}
                medium
            >
                {`(${contactsStore.searchResultCount})`}
            </BaseText>
        </>
    )
})
