import { StyleSheet } from 'react-native'

import { commonStyles } from 'styles'


export const styles = StyleSheet.create({
    title: commonStyles.largeHeading,

    subTitle: {
        ...commonStyles.secondaryText,
        marginLeft: 8,
    },
})
