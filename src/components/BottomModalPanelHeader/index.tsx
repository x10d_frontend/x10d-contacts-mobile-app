import React from 'react'
import { View, LayoutChangeEvent } from 'react-native'

import { observer, useLocalStore } from 'mobx-react'

import { BaseText } from 'common-components'

import { styles } from './styles'

interface Props {
    title : string

    closeModal : () => void
}


export const BottomModalPanelHeader : React.FC<Props> = observer((props) => {
    const store = useLocalStore(() => ({
        closeTextWidth: 0,

        handleCloseTextLayout: (event : LayoutChangeEvent) => {
            store.closeTextWidth = event.nativeEvent.layout.width
        },
    }))

    return (
        <View style={styles.container}>
            <BaseText
                medium
                style={styles.closeText}
                onPress={props.closeModal}
                onLayout={store.handleCloseTextLayout}
            >
                Отменить
            </BaseText>

            <BaseText
                bold
                style={styles.title}
            >
                {props.title}
            </BaseText>

            {/* Элемент, помогающий отрисовать заголовок
            модального окна по центру */}
            <View style={{ width: store.closeTextWidth }} />
        </View>
    )
})
