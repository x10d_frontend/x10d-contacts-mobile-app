import { StyleSheet } from 'react-native'

import { commonStyles, palette } from 'styles'


export const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 24,
    },

    closeText: {
        ...commonStyles.baseText,
        color: commonStyles.linkColor,
    },

    title: {
        fontSize: 15,
        lineHeight: 18,
        color: palette.capeCod,
    },
})
