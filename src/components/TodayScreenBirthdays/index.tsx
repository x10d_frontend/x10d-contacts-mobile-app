import React from 'react'
import { View, Image } from 'react-native'
import {
    filter,
    equals,
    ifElse,
    isNil,
    or,
    pipe,
    prop,
    sortBy,
    subtract,
    take,
    findIndex,
    length,
    F,
} from 'ramda'
import { isFuture, isToday } from 'date-fns'
import { set, getMonth } from 'date-fns/fp'

import { BaseText, BaseButton, BaseFloatingActionButton } from 'common-components'

import { useStores } from 'models'

import { birthday, currentMonthNumber, tagColor } from 'helpers'

import { IContactModel } from '../../models/contacts-store/contact'

import { styles } from './styles'

const baloonImage = require('../../../assets/images/baloons.png')
const contactStub = require('../../../assets/images/contact-stub.png')


const currentYearBirthday : (date : Date) => Date = set({
    year: new Date().getFullYear(),
})

const contactsWithFutureBirthdaysThisMonth : (
    items : Array<IContactModel>
) => Array<IContactModel> = pipe(
    filter(ifElse(
        pipe(
            prop('parsedBirthDate'),
            isNil,
        ),
        F,
        pipe(
            prop('parsedBirthDate'),
            currentYearBirthday,
            ifElse(
                pipe(
                    getMonth,
                    equals(currentMonthNumber),
                ),
                value => or(isToday(value), isFuture(value)),
                F,
            ),
        ),
    )),
    sortBy(pipe(
        prop('parsedBirthDate'),
        currentYearBirthday,
    )),
)

export const TodayScreenBirthdays : React.FC = () => {
    const CONTACTS_TO_RENDER = 3
    const TAGS_TO_RENDER = 2

    const { contactsStore } = useStores()
    const areThereBirthdays = contactsStore.birthdaysOfTheMonthCount !== 0
    const futureBirthdaysContacts = contactsWithFutureBirthdaysThisMonth(contactsStore.items)
    const estimatedBirthdaysCount = subtract(
        contactsStore.birthdaysOfTheMonthCount,
        length(futureBirthdaysContacts) > CONTACTS_TO_RENDER
            ? CONTACTS_TO_RENDER
            : length(futureBirthdaysContacts),
    )

    const getTagColor = pipe<string, number, string>(
        (tag : string) => findIndex(equals(tag), contactsStore.allTags),
        tagColor,
    )

    const addNewContact = () : void => {}

    return (
        <View style={styles.container}>
            <View style={[
                styles.header,
                areThereBirthdays ? { alignSelf: 'flex-start' } : null,
            ]}
            >
                <BaseText
                    bold
                    style={styles.headerTitle}
                >
                    Именинники месяца
                </BaseText>

                {areThereBirthdays && (
                    <BaseText style={styles.headerCount}>
                        {`(${contactsStore.birthdaysOfTheMonthCount})`}
                    </BaseText>
                )}
            </View>

            {areThereBirthdays
                ? (
                    <>
                        {take(CONTACTS_TO_RENDER, futureBirthdaysContacts).map(contact => (
                            <View
                                style={styles.contactContainer}
                                key={contact.id}
                            >
                                <Image
                                    style={styles.contactImage}
                                    source={contactStub}
                                />

                                <View style={styles.contactInfo}>
                                    <BaseText
                                        medium
                                        numberOfLines={1}
                                        ellipsizeMode="tail"
                                    >
                                        {contact.fullName}
                                    </BaseText>

                                    <View style={styles.contactInfoMeta}>
                                        {
                                            // Наличие даты гарантируется,
                                            // ранее уже отфильтровано
                                            // @ts-ignore
                                            isToday(currentYearBirthday(contact.parsedBirthDate))
                                                ? (
                                                    <BaseText
                                                        style={styles.contactBirthday__today}
                                                        medium
                                                    >
                                                        сегодня
                                                    </BaseText>
                                                )
                                                : (
                                                    <BaseText
                                                        style={styles.contactBirthday}
                                                        medium
                                                    >
                                                        {
                                                            // Наличие даты гарантируется,
                                                            // ранее уже отфильтровано
                                                            // @ts-ignore
                                                            birthday(contact.parsedBirthDate)
                                                        }
                                                    </BaseText>
                                                )
                                        }

                                        {contact.tags !== undefined &&
                                        take(TAGS_TO_RENDER, contact.tags)
                                            .map(tag => (
                                                <View
                                                    style={[
                                                        styles.contactTag,
                                                        { backgroundColor: getTagColor(tag) },
                                                    ]}
                                                    key={tag}
                                                >
                                                    <BaseText
                                                        style={styles.contactTagText}
                                                        medium
                                                        numberOfLines={1}
                                                        ellipsizeMode="tail"
                                                    >
                                                        {tag}
                                                    </BaseText>
                                                </View>
                                            ))
                                        }

                                        {contact.tags !== undefined &&
                                        contact.tags.length > TAGS_TO_RENDER && (
                                            <BaseText
                                                style={styles.moreTagsText}
                                                medium
                                            >
                                                {`ещё ${contact.tags.length - TAGS_TO_RENDER}`}
                                            </BaseText>
                                        )}
                                    </View>
                                </View>
                            </View>
                        ))}

                        {estimatedBirthdaysCount > 0 && (
                            <View style={styles.totalBirthdays}>
                                <View style={styles.totalBirthdaysCountContainer}>
                                    <BaseText
                                        style={styles.totalBirthdaysCount}
                                        medium
                                    >
                                        {estimatedBirthdaysCount}
                                    </BaseText>
                                </View>

                                <BaseText
                                    style={styles.contactInfo}
                                    medium
                                >
                                    Все именинники
                                </BaseText>
                            </View>
                        )}

                        <BaseFloatingActionButton
                            style={styles.floatingButton}
                            onPress={addNewContact}
                        />
                    </>
                )
                : (
                    <>
                        <BaseText
                            medium
                            style={styles.stubSubTitle}
                        >
                            В этом месяце нет именинников :(
                        </BaseText>

                        <Image
                            source={baloonImage}
                            style={styles.stubImage}
                        />

                        <BaseButton
                            style={styles.stubButton}
                            onPress={addNewContact}
                        >
                            <BaseText
                                bold
                                style={styles.stubButtonText}
                            >
                                Добавить
                            </BaseText>
                        </BaseButton>
                    </>
                )
            }
        </View>
    )
}
