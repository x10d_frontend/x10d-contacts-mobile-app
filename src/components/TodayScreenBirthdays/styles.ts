import { StyleSheet } from 'react-native'

import { commonStyles, palette } from 'styles'


const mainOffset = 24
const contactSize = 44

const contactContainer = {
    flexDirection: 'row' as 'row',
    alignItems: 'center' as 'center',
    alignSelf: 'stretch' as 'stretch',
    height: contactSize,
    marginTop: mainOffset * 2 / 3,
}

const imageContainer = {
    width: contactSize,
    height: contactSize,
    borderRadius: 8,
}

export const styles = StyleSheet.create({
    container: {
        position: 'relative',
        alignItems: 'center',
        width: '100%',
        marginTop: mainOffset,
        paddingTop: 20,
        paddingHorizontal: 20,
        paddingBottom: 28,
        backgroundColor: palette.white,
        borderRadius: commonStyles.mainButtonBorderRadius,
    },

    header: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: mainOffset / 2,
    },

    headerTitle: commonStyles.largeHeading,

    headerCount: {
        ...commonStyles.secondaryText,
        marginLeft: 10,
    },

    contactContainer,

    contactImage: imageContainer,

    contactInfo: {
        marginLeft: mainOffset / 2,
        flexShrink: 1,
    },

    contactInfoMeta: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 4,
    },

    contactBirthday: commonStyles.secondaryText,

    contactBirthday__today: {
        color: palette.chateauGreen,
    },

    contactTag: {
        maxWidth: mainOffset * 3,
        marginLeft: mainOffset / 3,
        paddingHorizontal: 6,
        paddingVertical: 2,
        // TODO Цвета
        backgroundColor: palette.bittersweet,
        borderRadius: 3,
    },

    contactTagText: {
        color: palette.white,
        fontSize: 11,
        lineHeight: 13,
    },

    moreTagsText: {
        marginLeft: mainOffset / 3,
        color: palette.silver,
        fontSize: 11,
        lineHeight: 13,
    },

    totalBirthdays: contactContainer,

    totalBirthdaysCountContainer: {
        ...imageContainer,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: palette.porcelain,
    },

    totalBirthdaysCount: {
        color: palette.chateauGreen,
        fontSize: 15,
        lineHeight: 18,
    },

    floatingButton: {
        right: 12,
        bottom: 12,
    },

    stubSubTitle: {
        ...commonStyles.baseText,
        maxWidth: commonStyles.deviceWidth * 0.4,
        marginTop: mainOffset / 2,
        textAlign: 'center',
    },

    stubImage: {
        marginTop: mainOffset,
        width: commonStyles.deviceWidth * 0.33,
        height: commonStyles.deviceWidth * 0.31,
    },

    stubButton: {
        width: commonStyles.deviceWidth * 0.46,
        marginTop: mainOffset,
    },

    stubButtonText: {
        ...commonStyles.baseText,
        color: palette.white,
    },
})
