import { StyleSheet } from 'react-native'

import { commonStyles, palette } from 'styles'


export const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    dayContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 45,
        height: 64,
    },

    dayContainer__active: {
        backgroundColor: palette.white,
        borderRadius: 22,
        borderColor: palette.yellowOrange,
        borderWidth: 2,
    },

    date: commonStyles.largeHeading,

    day: {
        marginTop: 2,
        color: palette.silver,
        fontSize: 13,
        lineHeight: 16,
    },
})
