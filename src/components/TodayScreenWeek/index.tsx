import React from 'react'
import { View } from 'react-native'
import { isSameDay } from 'date-fns'

import { BaseText } from 'common-components'

import { currentWeekDays, dayOfWeek } from 'helpers'

import { styles } from './styles'


export const TodayScreenWeek : React.FC = () => {
    return (
        <View style={styles.container}>
            {currentWeekDays.map(day => (
                <View
                    key={day.toDateString()}
                    style={[
                        styles.dayContainer,
                        isSameDay(day, new Date()) ? styles.dayContainer__active : null,
                    ]}
                >
                    <BaseText
                        medium
                        style={styles.date}
                    >
                        {day.getDate()}
                    </BaseText>

                    <BaseText style={styles.day}>
                        {dayOfWeek(day)}
                    </BaseText>
                </View>
            ))}
        </View>
    )
}
