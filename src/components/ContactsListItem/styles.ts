import { StyleSheet } from 'react-native'

import { commonStyles } from 'styles'


export const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 54,
        marginLeft: commonStyles.mainSidePadding,
        marginRight: commonStyles.mainSidePadding * 2,
    },

    image: {
        width: 37,
        height: 37,
    },

    text: {
        maxWidth: commonStyles.deviceWidth * 0.56,
        marginLeft: 17,
    },
})
