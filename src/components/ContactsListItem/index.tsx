import React from 'react'
import { Image, Text, TouchableOpacity } from 'react-native'

import { BaseText } from 'common-components'

import { IContactModel } from 'models/contacts-store/contact'

import { styles } from './styles'

const stubImage = require('../../../assets/images/contact-stub.png')

interface Props {
    item : IContactModel

    onPress : (item : IContactModel) => void
}


export const ContactsListItem : React.FC<Props> = (props) => {
    const { item } = props

    const isContainLastName = item.lastName !== undefined

    const onPress = () : void => {
        props.onPress(item)
    }

    return (
        <TouchableOpacity
            style={styles.container}
            onPress={onPress}
            activeOpacity={0.8}
        >
            <Image
                style={styles.image}
                source={stubImage}
            />

            <Text
                style={styles.text}
                numberOfLines={2}
                ellipsizeMode="tail"
            >
                {isContainLastName && (
                    <BaseText bold>
                        {`${item.lastName} `}
                    </BaseText>
                )}

                <BaseText
                    bold={!isContainLastName}
                    medium={isContainLastName}
                >
                    {item.firstName}
                </BaseText>

                {item.middleName !== undefined && (
                    <BaseText medium>
                        {` ${item.middleName}`}
                    </BaseText>
                )}
            </Text>
        </TouchableOpacity>
    )
}
