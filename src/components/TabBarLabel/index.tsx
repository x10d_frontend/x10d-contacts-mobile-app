import React from 'react'

import { BaseText } from 'common-components'

import { styles } from './styles'



interface ITabLabelProps {
    title : string
    color : string
}

export const TabLabel : React.FC<ITabLabelProps> = (props : ITabLabelProps) => (
    <BaseText
        medium
        style={[
            styles.label,
            { color: props.color },
        ]}
    >
        {props.title}
    </BaseText>
)
