import { StyleSheet } from 'react-native'


export const styles = StyleSheet.create({
    label: {
        marginBottom: 8,
        fontSize: 11,
        lineHeight: 13,
    },
})
