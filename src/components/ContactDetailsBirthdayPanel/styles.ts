import { StyleSheet } from 'react-native'

import { palette } from 'styles'


export const styles = StyleSheet.create({
    zodiacImageContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 40,
        height: 40,
        borderRadius: 40 / 2,
        backgroundColor: palette.yellowOrange,
    },

    zodiacImage: {
        width: 26,
        height: 26,
    },

    birthday: {
        marginTop: 8,
        fontSize: 22,
        lineHeight: 27,
    },

    turnsYearsOld: {
        marginTop: 2,
        color: palette.manatee,
    },

    remind: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        marginTop: 20,
        paddingTop: 10,
        paddingHorizontal: 20,
        borderTopWidth: 1,
        borderTopColor: palette.selago,
    },

    remindBell: {
        width: 22,
        height: 22,
    },

    remindLabel: {
        marginLeft: 20,
    },

    remindSwitch: {
        marginLeft: 'auto',
    },
})
