import React from 'react'
import { Image, View, TouchableOpacity } from 'react-native'
import { observer, useLocalStore } from 'mobx-react'
import { pipe, prop } from 'ramda'
import { sub } from 'date-fns'

import { BaseModal, BaseSwitch, BaseText } from 'common-components'

import { zodiacSignByDate } from 'services'
import { birthday, distanceToNow } from 'helpers'

import { ContactDetailsRemindDatePicker, IPickerItem } from '../ContactDetailsRemindDatePicker'

import { styles } from './styles'

const bellIcon = require('../../../assets/icons/bell.png')


interface Props {
    birthDate : Date

    setRemindDate : (remindDate : Date | undefined) => void

    remindDate ?: Date
}


/**
 * Функция, возвращающая текст, говорящий сколько лет
 * исполнится человеку в его ближайший день Рождения
 *
 * @example
 * turnsYearsOld(02-11-1993) // исполнится 27 лет
 */
const turnsYearsOld = pipe(
    distanceToNow,
    value => `исполняется ${value}`,
)


export const ContactDetailsBirthdayPanel : React.FC<Props> = observer((props) => {
    const localStore = useLocalStore(source => ({
        pickerItems: [
            {
                title: 'В тот же день',
                value: source.birthDate,
            },
            {
                title: 'За 1 день',
                value: sub(source.birthDate, { days: 1 }),
            },
            {
                title: 'За 2 дня',
                value: sub(source.birthDate, { days: 2 }),
            },
            {
                title: 'За неделю',
                value: sub(source.birthDate, { weeks: 1 }),
            },
            {
                title: 'Без напоминания',
                value: undefined,
            },
        ],

        get maybeZodiac() {
            return zodiacSignByDate(source.birthDate)
        },
        get turnsYearsOldText() {
            return turnsYearsOld(source.birthDate)
        },
        get birthdayText() {
            return birthday(source.birthDate)
        },

        get isRemindDateExists() {
            return source.remindDate !== undefined
        },
        get remindText() {
            if (source.remindDate === undefined) {
                return 'Уведомлять меня'
            }

            return this.pickerItems
                .filter(item => item.value !== undefined)
                // Гарантируется наличие нужных полей.
                // @ts-ignore
                .find(item => item.value.valueOf() === source.remindDate.valueOf())
                ?.title || 'Уведомлять меня'
        },

        async showRemindDatePicker() : Promise<void> {
            await BaseModal.show((closeModal) => {
                const handleSelecting = pipe(
                    prop<'value', IPickerItem['value']>('value'),
                    source.setRemindDate,
                    closeModal,
                )

                return {
                    children: <ContactDetailsRemindDatePicker
                        items={this.pickerItems}
                        onSelect={handleSelecting}
                        closeModal={closeModal}
                    />,
                    withHandle: false,
                }
            })
        },
    }), props)

    return (
        <>
            {localStore.maybeZodiac.mapOrDefault(
                value => (
                    <View style={styles.zodiacImageContainer}>
                        <Image
                            source={value.image}
                            style={styles.zodiacImage}
                        />
                    </View>
                ),
                null,
            )}

            <BaseText
                medium
                style={styles.birthday}
            >
                {localStore.birthdayText}
            </BaseText>

            <BaseText
                medium
                style={styles.turnsYearsOld}
            >
                {localStore.turnsYearsOldText}
            </BaseText>

            <TouchableOpacity
                style={styles.remind}
                activeOpacity={0.8}
                onPress={localStore.showRemindDatePicker}
            >
                <Image
                    style={styles.remindBell}
                    source={bellIcon}
                />

                <BaseText
                    medium
                    style={styles.remindLabel}
                >
                    {localStore.remindText}
                </BaseText>

                <BaseSwitch
                    style={styles.remindSwitch}
                    value={localStore.isRemindDateExists}
                    disabled
                />
            </TouchableOpacity>
        </>
    )
})
