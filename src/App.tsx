import React, { useEffect, useState } from 'react'
import RNBootSplash from 'react-native-bootsplash'

import { BaseLoader, BaseModalProvider } from 'common-components'

import { RootStoreProvider, RootStore, setupRootStore } from 'models'

import { RootNavigator } from 'navigation'


const App : React.FC = () => {
    const [rootStore, setRootStore] = useState<RootStore | null>(null)

    useEffect(() => {
        (async () => {
            const store = await setupRootStore()

            setRootStore(store)

            RNBootSplash.hide()
        })()
    }, [])

    return rootStore !== null
        ? (
            <RootStoreProvider value={rootStore}>
                <RootNavigator />

                <BaseModalProvider />
            </RootStoreProvider>
        )
        : <BaseLoader />
}

export default App
